#!/bin/bash
set -e

# Check if the first parameter exists
if [ -z "$1" ]; then
  OUTPUT_FILE="radiobrowser-dist.tar.gz"
else
  OUTPUT_FILE="$1"
fi

DISTDIR=$(pwd)/dist
mkdir -p ${DISTDIR}
mkdir -p ${DISTDIR}/bin
cargo build --release

cp target/release/radiobrowser-api-rust ${DISTDIR}/bin/radiobrowser
mkdir -p ${DISTDIR}/init
cp debian/radiobrowser.service ${DISTDIR}/init/
cp -R static ${DISTDIR}/
cp -R etc ${DISTDIR}/
cp install_from_dist.sh ${DISTDIR}/install.sh

tar -czf $(pwd)/${OUTPUT_FILE} -C ${DISTDIR} bin init static etc install.sh